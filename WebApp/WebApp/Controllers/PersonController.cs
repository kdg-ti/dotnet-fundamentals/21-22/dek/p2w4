﻿using System;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    // https://localhost:5001/Person
    public class PersonController : Controller
    {
        // GET
        public IActionResult Index(Person p)
        {
            if (String.IsNullOrEmpty(p.Lastname))
                p = new Person() { Firstname = "Jan", Lastname = "Janssens" };
            
            // (1) ViewData-/ViewBag-data
            ViewData["Firstname"] = p.Firstname;
            ViewBag.Lastname = p.Lastname;
            
            // (2) Model-data
            return View(p);
        }

        [HttpGet]
        public IActionResult Edit(int id, Person person)
        {
            // data ophalen obv id
            
            return View(person);
        }
        [HttpPost]
        public IActionResult Edit(Person person)
        {
            if (!ModelState.IsValid)
            {
                return View(person);
            }
            
            // persisteer
            
            //return View("Index", person);
            return RedirectToAction("Index", person);
        }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Person
    {
        public string Firstname { get; set; }
        [StringLength(50, MinimumLength = 3)]
        public string Lastname { get; set; }
    }
}